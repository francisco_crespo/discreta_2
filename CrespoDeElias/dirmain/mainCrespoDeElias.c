#include "Cthulhu.h"

int main() {

    NimheP grafo = NuevoNimhe();

    if (grafo == NULL) {
        return 0;
    }

    u32 n = NumeroDeVertices(grafo);
    u32 m = NumeroDeLados(grafo);
    printf("Vertices: %u y Lados: %u \n", n, m);

    int dos = Chidos(grafo);
    if(dos == 1) {

        printf("Es dos coloreable\n");

    } else {

        printf("No es dos coloreable\n");

        u32 colores = 0;
        //ImprimirOrdenArray(grafo);
        //printf("--------------------------------------------------\n");
      //   for(u32 i = 0; i < 1000; i++) {
      //       colores = Greedy(grafo);
      //       printf("Cantidad de colores con orden aleatorio %u: %u \n", i, colores);
      //       OrdenAleatorio(grafo, 50);
      //   }
        //printf("--------------------------------------------------\n");
        //ImprimirOrdenArray(grafo);
        //printf("--------------------------------------------------\n");

      //   OrdenWelshPowell(grafo);
        GrandeChico(grafo);
        colores = Greedy(grafo);
        printf("Cantidad de colores con GrandeChico: %u \n", colores);
        ImprimirOrdenArray(grafo);
    }

    DestruirNimhe(grafo);


    return 0;
}
