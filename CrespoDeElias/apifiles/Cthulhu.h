#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <time.h>


typedef struct Cola *ColaP;
typedef struct Vecino VecinoSt;
typedef struct Vertice VerticeSt;
typedef unsigned int u32;
typedef struct NimheSt *NimheP;

typedef struct Lado LadoSt;
typedef struct Lados *LadosP;

typedef struct Orden OrdenSt;

/*
    Un vecino se caracteriza por el nombre y la posicion en el array de vertices. Esa posicion
    nos servira para ver el color de este o cualquier otra caracteristica que pueda tener un vertice.
*/

struct Vecino {
    u32 nombre;
    u32 posicionVertices;
};

/*
    Es la estructura que tiene cada uno de los vertices del grafo. Un vertice está
    caracterizado por un nombre, una posicion en el array de vertices, un color y un grado.
    Ademas cada vértice apunta hacia un array de vecinos de este.
*/

struct Vertice {
    u32 nombre;
    u32 grado;
    u32 color;
    u32 posicion;
    VecinoSt *vecinos;
};

/*
    Esta Estructura es la que guarda toda la información del grafo. Se trata de
    una estructura que contiene el array de vertices ademas de una serie de campos
    que gusrdan caracteristicas del grafo, como #lados, #vertices y #colores.
*/
struct NimheSt {
    u32 numeroVertices;
    u32 numeroLados;
    u32 longitudActual;
    u32 cantidadColores;
    OrdenSt *arrayOrden;
    VerticeSt *vertices;
};

/*
    En esta estructura guardamos el orden de un momento dado. El grafo sera recorrido de acuerdo a este orden
*/

struct Orden {
    u32 nombre;
    u32 grado;
    u32 color;
    u32 posicion;
    u32 vmismocolor;
};

/*
    Este tipo de estructura será el tipo de campo celdilla que poseerá la estructura lados.
    Se trata de una estructura auxiliar que servira para llenar el grafo de forma lineal, o sea,
    necesaria para bajar la complejidad de la lectura del grafo a partir de stdin.
*/

struct Lado {
    u32 izquierda;
    u32 derecha;
    u32 posicion;
};

/*
    Estructura auxiliar lados es la que recivirá los datos de la entrada estandar.
    Pasos:
        -Primero se llenan los campos derecho e izquierdo hasta 2m. O sea, primero tendremos
         los m vertices izquierdos como izquierdos y derechos como derechos y a partir de m se
         invierten las posiciones. Asi nos aseguramos de captar la total cantidad de vecinos
         de un determinado vertice.

        -Despues se ordena la estructura teniendo solo en cuenta el campo lado izquierda.
         Este paso es para obtener el grado de cada vertice y la posicion que ocupara en
         el array vertices del grafo. Ademas sus vecinos quedaran reflejados en la fila
         lado derecha.

        -Tercero, se ordena el array en funcion de lado derecha. Esto se hace para que
         queden refeljados la posicion y el nombre de los vecinos en la pirmera y
         segunda fila. Despues se recorre y se van rellenando los array vecinos con
         nombre y posicion.
*/

struct Lados {
    u32 nroLados;
    u32 nroActual;
    LadoSt *arrayLados;
};

/*
   Estructura de la cola
*/

struct Cola{
    ColaP next;
    VerticeSt elem;
};






						//FUNCIONES DE GRAFO:

/*
	Reserva un espacio de memoria para la estructura NimheSt y devuelve un puntero a ese espacio.
	Lee un grafo desde standard input y llena la estructura NimheSt con los datos.
	Inicializa los vertices sin color "0".

	Devuelve null en caso de error (fallo en alocar memória, formato de entrada inválido...).
*/
NimheP NuevoNimhe();

/*
	Destruye G y libera la memoria alocada. Retorna 1 si todo anduvo bien y 0 si no.
*/
void DestruirNimhe(NimheP G);


						//FUNCIONES DE VERTICES:

/*
	Devuelve el color con el que está coloreado el vértice x.
*/
u32 ColorDelVertice(VerticeSt x);

/*
	Devuelve el grado del vértice dado.
*/
u32 GradoDelVertice(VerticeSt x);

/*
	Devuelve el nombre del vértice dado.
*/
u32 NombreDelVertice(VerticeSt x);

/*
	Devuelve la posicion de un vertice dado en el grafo.
*/
u32 PosicionDelVertice(VerticeSt x);

/*
	Devuelve la cantidad de vecinos de un vertice en un grafo dado.
*/
void ImprimirVecinosDelVertice(VerticeSt x, NimheP G);

/*
	Devuelve el número de vértices del grafo G.
*/
u32 NumeroDeVertices(NimheP G);

/*
	Devuelve el número de lados del grafo G.
*/
u32 NumeroDeLados(NimheP G);

/*
	Retorna el número de vértices de color i. (si i = 0 devuelve el número de vertices no coloreados).
*/
u32 NumeroVerticesDeColor(NimheP G, u32 i);

/*
	Imprime una linea que dice “Vertices de Color i:” y a continuacion una lista de los vertices que
	tienen el color i, separados por comas y con un punto luego del último color. (si i = 0 esta lista
	será la lista de vertices no coloreados).
	Los vertices no tienen porque estar ordenados.
	Si no hay vertices de color i debe imprimir “No hay vertices de color i”
*/
void ImprimirVerticesDeColor(NimheP G, u32 i);

/*
	Devuelve la cantidad de colores usados en el coloreo de G.
*/
u32 CantidadDeColores(NimheP G);

/*
	Devuelve el vértice numero i en el orden guardado en ese momento en G. (el índice 0 indica el
	primer vértice, el índice 1 el segundo, etc)
*/
VerticeSt IesimoVerticeEnElOrden(NimheP G, u32 i);

/*
	Devuelve el vecino numero i de x en el orden en que está guardado uds. en G. (el orden
	es irrelevante, lo importante es que de esta forma podemos pedir externamente la lista de vecinos)
	(el índice 0 indica el primer vértice, el índice 1 el segundo, etc)
*/
VerticeSt IesimoVecino(NimheP G, VerticeSt x, u32 i);


/*
	Ordena los vertices en orden creciente de sus “nombres” reales. (recordar que el nombre real de
	un vértice es un u32)
*/
void OrdenNatural(NimheP G);

/*

	Ordena los vertices de G de acuerdo con el orden Welsh-Powell, es decir, con los grados en orden
	no creciente.
*/
void OrdenWelshPowell(NimheP G);

/*
    Esta función ordena el arrayOreden de la estructura NimheSt de forma aleatoria.
*/
void OrdenAleatorio(NimheP G, u32 intercambios);

/*
    Imprime el orden del array orden segun el nombre de los vértices. Esta funcion sirve como chequeo
    para saber si el orden esta correcto.
*/

void GrandeChico(NimheP G);

void ChicoGrande(NimheP G);


void ImprimirOrdenArray(NimheP G);


u32 Greedy(NimheP G);

int Chidos(NimheP G);



/*
     Funciones de la cola

*/

ColaP vacia(void);
/*Devuelve una cola vacia.
 *El llamador deve destruir la cola cuando deje de usarla.
 */

ColaP decolar(ColaP C);
/*Devuelve una cola sin el primer elemento,
 *la lista no debe de ser vacia.
 */

ColaP encolar(ColaP C, VerticeSt elem);
/*Devuelve una cola con un elemento mas,
 *la cola no debe de estar llena.
 */

VerticeSt primero(ColaP C);
/*Devuelve el primer elemento de la cola,
 *la cola no debe de ser vacia*/

bool es_vacia(ColaP C);
/*devuelve verdadero si la cola esta vacia*/

void printCola(ColaP C);
/*imprime la cola*/

void destruirCola(ColaP P);
/*destruye la cola*/
