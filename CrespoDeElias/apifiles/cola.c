#include"Cthulhu.h"

ColaP vacia(void) {
    return NULL;
}

ColaP decolar(ColaP C) {
    ColaP q = NULL;
    if (C->next == q) {
        free(C);
        C = NULL;
    } else {
        q = C;
        C = C->next;
        free(q);
        q = NULL;
    }
    return C;
}

ColaP encolar(ColaP C, VerticeSt elem) {
    ColaP q = NULL;
    ColaP r = NULL;
    q = calloc(1, sizeof(struct Cola));
    q->elem = elem;
    q->next = NULL;

    if (C == NULL) {
        C = q;
    } else {
        r = C;
        while(r->next != NULL) {
            r = r->next;
        }
        r->next = q;
    }
    return C;
}

VerticeSt primero(ColaP C) {
    return C->elem;
}

bool es_vacia(ColaP C) {
    return (C == NULL);
}

void destruirCola(ColaP C) {
    while(C->next != NULL) {
        free(C->next);
        C->next = NULL;
    }
    free(C);
    C = NULL;
}

void printCola(ColaP C) {
    u32 nombre;
    ColaP temp = C;
    while(temp != NULL) {
        nombre = NombreDelVertice(temp->elem);
        printf("%u ", nombre);
        temp = temp->next;
    }
    printf("\n");
}
