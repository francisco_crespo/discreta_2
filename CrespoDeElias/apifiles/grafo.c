#include "Cthulhu.h"


/*------------------FUNCIONES AUXILIARES:-----------------------------------*/

VerticeSt obtenerVertice(NimheP grafo, u32 nombre);

void guardarVertice(NimheP grafo, VerticeSt nombre, u32 pos);

VerticeSt crearVertice(u32 nombre, u32 pos, u32 grado);

void DestruirLados(LadosP l);

int cmpIzquierdos(const void *a, const void *b);

int cmpDerechos(const void *a, const void *b);

int cmpNombre(const void *a, const void *b);

int cmpGradoDecreciente(const void *a, const void *b);

int cmpCantColorCreciente(const void *a, const void *b);

int cmpCantColorDecreciente(const void *a, const void *b);

void GuardarOrden (NimheP grafo, OrdenSt orden, u32 pos);

void llenarArrayOrden(NimheP grafo);

int cmpColor(const void *a, const void *b);

int cmpColorOrden(const void *a, const void *b);

int cmpColorDecreciente(const void *a, const void *b);

void DescolorearVertices(NimheP G);

void intercambioPosiciones(NimheP G, u32 i, u32 j);

/*--------------------------------------------------------------------------*/


NimheP NuevoNimhe() {

    char str[81]; // bufer que almacena cada linea de DIMACS
    int offset = 0; // usado para retroceder en la lectura
    NimheP grafo = NULL;
    u32 izq, der, n, m; // Variables usadas para la lectura.
    LadosP lados = NULL;

    grafo = calloc(1, sizeof(struct NimheSt));

    lados = calloc(1, sizeof(struct Lados));

    if(lados == NULL || grafo == NULL) {
        return NULL;
    }

    while(fgets(str, 81, stdin) != NULL) {

        //No tenemos en cuenta las primeras lineas de comentarios
        if(str[0] == 'c') {
            offset = ftell(stdin);
            continue;
        }

        fseek(stdin, offset + 1, SEEK_SET);
        //Comprobamos si existe p, como linea de entrada.
        if(str[0] != 'p') {

            offset = ftell(stdin);
            printf("%s\n", "Error en la entrada de datos, falta la linea p.");
            return NULL;

        } else {

            fseek(stdin, offset + 1, SEEK_SET);
            /*
              Tomamos los datos de la linea p y asignamos memoria al array vertices del grafo
              y al array lado de la estructura auxiliar lados
            */
            if(fscanf(stdin, "%s %u %u", str, &n, &m) == 3) {

                grafo->numeroVertices = n;
                grafo->numeroLados = m;
                grafo->longitudActual = 0; //para llevar el indice donde guardamos.
                //Aqui pedimos memoria para el array que guardará los vértices.
                grafo->vertices = calloc(grafo->numeroVertices, sizeof(struct Vertice));
                //Aqui pedimos memoria para el array donde guardaremos el orden en el que recorreremos el grafo
                grafo->arrayOrden = calloc(grafo->numeroVertices + 1, sizeof(struct Orden));

                lados->nroLados = m;
                lados->nroActual = 0; //para llevar el indice donde guardamos.
                //memoria para la estructura auxiliar lados.
                lados->arrayLados = calloc(2*lados->nroLados, sizeof(struct Lado));

                if(grafo->vertices == NULL || lados->arrayLados == NULL) {
                    return NULL;
                }
            }
            break;
        }
    }

    VerticeSt vertice;
    VecinoSt vecino;
    LadoSt lado;
    u32 vIzquierdo, vPosicion, vGrado;

   while (fscanf(stdin, "%s %u %u", str, &izq, &der) == 3 && lados->nroActual < lados->nroLados) {

        /*
            Esta es la parte donde se llena la estructura auxiliar lados desde 0 a 2m;
        */

        lado.izquierda = izq;
        lado.derecha = der;
        lados->arrayLados[lados->nroActual] = lado;

        lado.izquierda = der;
        lado.derecha = izq;
        lados->arrayLados[lados->nroLados + lados->nroActual] = lado;

        lados->nroActual++;
    }


    //aqui ordenaremos lados en función del campo izquierda.
    qsort(lados->arrayLados, 2*lados->nroLados , sizeof(struct Lado), cmpIzquierdos);


    /* Aqui recorremos el array auxiliar lados y vamos fijandonos en el campo izquierda para
        ir completando el campo posición y calculando el grado de cada vertice.
        A su vez iremos añadiendo el vertice creado, a partir de los datos del array lados,
        al array vertices del grafo.
    */

    vPosicion = 0;
    vGrado = 0;

    for(u32 i = 0; i < 2*lados->nroLados; i++) {

        lado = lados->arrayLados[i]; //obtenemos el lado del array lados
        vIzquierdo = lado.izquierda; //nos fijamos en el nombre del vertice izquierdo

        if(i != 2*lados->nroLados - 1) {

            if(vIzquierdo == lados->arrayLados[i + 1].izquierda) { // si el nombre es igual al siguiente

                lado.posicion = vPosicion; //le damos posicion al vertice
                lados->arrayLados[i] = lado; // lo gurdamos de nuevo en el array lados
                vGrado++; // aumentamos el grado del vertice
                // El campo derecha no es afectado en este for

            } else {

                lado.posicion = vPosicion;
                lados->arrayLados[i] = lado;
                vGrado++;

                //aqui tenemos que crear el vertice, dotandolo de un nombre, una posicion y un grado
                vertice = crearVertice(vIzquierdo, vPosicion, vGrado);

                //una vez creado el vertice los guardamos en el grafo.
                guardarVertice(grafo, vertice, vPosicion);

                //aca aumentamos la posición y ponemos el grado a 0 antes de pasar a un nuevo vertice.
                vPosicion++;
                vGrado = 0;
            }

        } else {

            if(vIzquierdo == lados->arrayLados[i - 1].izquierda) { // si el nombre es igual al anterior

                lado.posicion = vPosicion; //le damos posicion al vertice
                lados->arrayLados[i] = lado; // lo gurdamos de nuevo en el array lados
                vGrado++; // aumentamos el grado del vertice
                vertice = crearVertice(vIzquierdo, vPosicion, vGrado);
                guardarVertice(grafo, vertice, vPosicion);
                // El campo derecha no es afectado en este for
                vPosicion = 0;
                vGrado = 0;

            } else {

                //vPosicion++;
                lado.posicion = vPosicion;
                lados->arrayLados[i] = lado;
                vGrado = 1;

                //aqui tenemos que crear el vertice, dotandolo de un nombre, una posicion y un grado
                vertice = crearVertice(vIzquierdo, vPosicion, vGrado);

                //una vez creado el vertice los guardamos en el grafo.
                guardarVertice(grafo, vertice, vPosicion);

                //ya no cambiaran mas la posicion y el grado ya que era el ultimo vertice
                vPosicion = 0;
                vGrado = 0;
            }
        }
    }

    //Aqui ordenamos lado en funcion del campo derecha
    qsort(lados->arrayLados, 2*lados->nroLados , sizeof(struct Lado), cmpDerechos);

    //Aqui iremos obteniendo los vecinos y su posicion de array auxiliar lados y los iremos añadiendo al grafo
    u32 w = 0; //sirve para reccores el array auxiliar lados
    for (u32 z = 0; z < grafo->numeroVertices; z++) {

        for (u32 r = 0; r < grafo->vertices[z].grado; r++) {

            lado = lados->arrayLados[w]; //obtenemos el lado del array lados

            vecino.nombre = lado.izquierda; // de lados.izquierda sacamos el nombre del vecino
            vecino.posicionVertices = lado.posicion; // de lados.posicion ontenemos la posicion
            grafo->vertices[z].vecinos[r] = vecino; // guardamos el vecino del vertice de la posicion z en la aposicion r de vecinos.
            w++;
        }
    }

    llenarArrayOrden(grafo);

    /*for (u32 r = 0; r < grafo->numeroVertices; r++) {
        printf("Vertice %u de Grado %u ", grafo->vertices[r].nombre, grafo->vertices[r].grado);
        for (u32 j = 0; j < grafo->vertices[r].grado; j++) {
            printf(" y Vecinos %u, Posicion %u", grafo->vertices[r].vecinos[j].nombre, grafo->vertices[r].vecinos[j].posicionVertices);
        }
        printf("\n");
    }*/

    DestruirLados(lados);

    //verifica la existencia de vertices aislados en la entrada dada.
    if (grafo->numeroVertices != grafo->longitudActual) {
        printf("%s\n", "Error: n* != n");
        return NULL;
    }

    return grafo;
}

void DestruirNimhe(NimheP G) {

    u32 i = 0;
    u32 n = G->numeroVertices;
    while (i < n) {
        free(G->vertices[i].vecinos);
        G->vertices[i].vecinos = NULL;
        i++;
    }
    free(G->vertices);
    G->vertices = NULL;
    free(G->arrayOrden);
    G->arrayOrden = NULL;
    free(G);
    G = NULL;
}


u32 ColorDelVertice(VerticeSt x) {
    return x.color;
}

u32 GradoDelVertice(VerticeSt x) {
    return x.grado;
}

u32 NombreDelVertice(VerticeSt x) {
    return x.nombre;
}

u32 PosicionDelVertice(VerticeSt x) {
    return x.posicion;
}

void ImprimirVecinosDelVertice(VerticeSt x, NimheP G) {

    u32 posicion = x.posicion;
    printf("{ ");
    for (u32 i = 0; i < G->vertices[posicion].grado; i++) {
        printf("%u, ", G->vertices[posicion].vecinos[i].nombre);
    }
    printf("}\n");
}

u32 NumeroDeVertices(NimheP G) {
    return G->numeroVertices;
}

u32 NumeroDeLados(NimheP G) {
    return G->numeroLados;
}

u32 NumeroVerticesDeColor(NimheP G, u32 i) {

    u32 contador = 0;
    for (u32 j = 0; j < G->numeroVertices; j++) {
        if (i == G->vertices[j].color) {
            contador++;
        }
    }
    return contador;
}

void ImprimirVerticesDeColor(NimheP G, u32 i) {
    bool b = false;

    printf("Vertices de color %u: { ", i);
    for (u32 j = 0; j < G->numeroVertices; j++) {
        if (i == G->vertices[j].color) {
            printf("%u, ", G->vertices[j].nombre);
            b = true;
        }
    }
    printf("}\n");

    if (b == false) {
        printf("No hay vertices de color %u\n", i);
    }
}

u32 CantidadDeColores(NimheP G) {
    return G->cantidadColores;
}

VerticeSt IesimoVerticeEnElOrden(NimheP G, u32 i) {
    //primero obtiene el nodo del array orden, despues se fija en la posicion,
    //que es la posicion que ocupa en el array vertices, y con eso obtiene el vertice

    VerticeSt vertice;
    vertice = G->vertices[G->arrayOrden[i].posicion];
    return vertice;
}

VerticeSt IesimoVecino(NimheP G, VerticeSt x, u32 i) {

    VerticeSt vertice;
    u32 posicion;
    posicion = G->vertices[x.posicion].vecinos[i].posicionVertices;
    vertice = G->vertices[posicion];

    return vertice;
}


/*-------------------FUNCIONES PARA ORDENAR VERTICES----------------------------------------*/

/*
    Esta funcion ordena los vertices por su orden natural. La primera vez no será necesario usarla
    ya que el array orden vendra ordenado de entrada.
*/
void OrdenNatural(NimheP G) {

    //usamos qsot de la libreria de c para ordenar el array orden.
    qsort(G->arrayOrden, G->numeroVertices , sizeof(struct Orden), cmpNombre);
}


void OrdenWelshPowell(NimheP G) {
    qsort(G->arrayOrden, G->numeroVertices , sizeof(struct Orden), cmpGradoDecreciente);
}

void OrdenAleatorio(NimheP G, u32 intercambios) {
    srand(time(NULL)); //Genera una semilla para un numero aleatorio.
    u32 x = 0; //Estas seran las posiciones aleatorias de intercambio en el array.
    u32 y = 0;

    for (u32 i = 0; i < intercambios; ++i) {
        x = rand() % G->numeroVertices;
        y = rand() % G->numeroVertices;
        intercambioPosiciones(G, x, y);
    }
}

void GrandeChico(NimheP G){

   llenarArrayOrden(G);

   qsort(G->arrayOrden, G->numeroVertices, sizeof(struct Orden), cmpColorOrden);

   u32 i=0;
   u32 suma =0;
   u32 posparcial=0;

   while (i<G->numeroVertices){
      if (G->arrayOrden[i].color == G->arrayOrden[i+1].color){
         suma++;
      } else {
         G->arrayOrden[i].vmismocolor = suma;
         for (u32 k = posparcial; k < i; k++){
            G->arrayOrden[k].vmismocolor = suma;
         }
         posparcial = i;

         suma = 0;
      }
      i++;
   }

   qsort(G->arrayOrden, G->numeroVertices, sizeof(struct Orden), cmpCantColorDecreciente);

}


void ChicoGrande(NimheP G){

   qsort(G->arrayOrden, G->numeroVertices, sizeof(struct Orden), cmpColorOrden);

   u32 i=0;
   u32 suma =0;
   u32 posparcial=0;

   while (i<G->numeroVertices){
      if (G->arrayOrden[i].color == G->arrayOrden[i+1].color){
         suma++;
      } else {
         G->arrayOrden[i].vmismocolor = suma;
         for (u32 k = posparcial; k < i; k++){
            G->arrayOrden[k].vmismocolor = suma;
         }
         posparcial = i;

         suma = 0;
      }
      i++;
   }

   qsort(G->arrayOrden, G->numeroVertices, sizeof(struct Orden), cmpCantColorCreciente);

}


/*------------------FUNCIONES DE COLOREO------------------------------------------------------*/

u32 Greedy(NimheP G){

    u32 *listacolores = calloc(1000, sizeof(u32)); //Este es un array donde guardaremos los colores

    VerticeSt vertice, vecino;
    OrdenSt nodo;
    u32 posvecino;
    u32 *arrayColores;
    u32 diferencia;
    DescolorearVertices(G);

    for(u32 i = 0; i < G->numeroVertices; i++) {

        u32 cantidadColores = 0; //Sirve para ver cuantos vecinos hay coloreados.
        /*Obtenemos el nodo que va a ser coloreado del array posicion. Este array ira cambiando su orden
          a medida que avanza el programa.*/
        nodo = G->arrayOrden[i];
        //El nodo guarda la posición que este tiene en el array vertices. Y de ahi sacamos el vertice.
        vertice = G->vertices[nodo.posicion];
        //Creamos un array colores para ir gardando los colores de los vecinos.
        arrayColores = calloc(vertice.grado + 1, sizeof(u32));

        for(u32 j = 0; j < vertice.grado; j++){ //Recorre los vecinos y se fija si estan coloreados
            posvecino = vertice.vecinos[j].posicionVertices; //obtiene el vecino y se fija en la posicion que ocupa en el array vertices
            vecino = G->vertices[posvecino]; //obtenemos el vertice vecino

            if (vecino.color != 0){
                arrayColores[cantidadColores] = vecino.color; //Si tiene color lo agregamos al array colores
                cantidadColores++;
            }
        }

        qsort(arrayColores, cantidadColores, sizeof(u32), cmpColor);


        /*Si ningun vecino tiene color le damos a nuestro vertice el color 1
        Ademas, como el array colores esta ordenado y no se repiten los colores, si el primer
        color es distinto de 1 tambien le datemos color 1 a nuestro vertice*/
        if(cantidadColores == 0 || arrayColores[0] != 1) {

            vertice.color = 1;
            nodo.color = vertice.color;
            listacolores[vertice.color] = vertice.color;

        } else { //si la cantidad de colores es != 0 ordenmos el array colores de forma ascendente y buscamos en el.

            //El color de nuestro vertice será el color entre dos colores no cosecutivos, de nuestro array de colores auxiliar.
            for(u32 k = 0; k < vertice.grado; k++) {

                diferencia = arrayColores[k + 1] - arrayColores[k];
                //Si la diferencia es mayor a uno asignamos el color mas pequeño + 1
                if(diferencia > 1) {
                    vertice.color = arrayColores[k] + 1;
                    nodo.color = vertice.color;
                    listacolores[vertice.color] = vertice.color;
                    break;
                }
                diferencia = 0;
            }
        }

        G->vertices[vertice.posicion] = vertice;
        G->arrayOrden[i] = nodo;

        free(arrayColores);
        arrayColores = NULL;
    }
    //FINAL DEL COLOREO

    /*for(u32 i = 0; i < G->numeroVertices; i++) {
        printf("Vertice: %u, Color: %u\n", G->vertices[i].nombre, G->vertices[i].color);
    }*/

    //Ahora recorreremos la lista de colores para ir contando la cantidad de colores que tenemos.
    u32 i = 0;
    while(listacolores[i + 1] != 0) {
        i++;
    }

    free(listacolores);
    listacolores = NULL;

    return i;
}


int Chidos(NimheP G) {

    u32 verticesColoreados = 0;
    u32 indice = 0; //aumenta cuando el un determinado vertice esta coloreado u no se puede seleccionar de nuevo.
    u32 posVecino;
    VerticeSt vertice, vecino;
    DescolorearVertices(G);

    while (verticesColoreados < G->numeroVertices) {
        //Si el vertice no esta coloreado, entonces lo coloreamos
        if(G->vertices[indice].color == 0) {

            vertice = G->vertices[indice];
            vertice.color = 1;
            verticesColoreados++;
            G->vertices[vertice.posicion] = vertice; //lo guaramos en la estructura con su modificacion.

        } else { //Si el vertice tiene color, lo pasamos.
            indice++;
            continue;
        }
        ColaP cola = vacia();
        cola = encolar(cola, vertice); //Encolamos el vertice para procesarlo en el siguiente while.

        while(!es_vacia(cola)) { //no paramos hasta que la cola no queda vacia. i.e. hasta que la componente conexa este coloreada.

            vertice = primero(cola); //Obtenemos el vertice de la cola
            cola = decolar(cola); //lo sacamos para no volverlo a tomar de nuevo

            for(u32 i = 0; i < vertice.grado; i++) { //Recorremos los vecinos de dicho vertice.
                posVecino = G->vertices[vertice.posicion].vecinos[i].posicionVertices;
                vecino = G->vertices[posVecino]; //obtenemos el vecino, en el array de vertices, para ver su color

                if(vecino.color == 0) { //Si el vecino no tiene color lo encolamos y le damos color
                    vecino.color = 3 - vertice.color;
                    cola = encolar(cola, vecino);
                    verticesColoreados++;
                }
                G->vertices[posVecino] = vecino;
            }
        }
    }
    for(u32 z = 0; z < G->numeroVertices; z++) { //Combrobamos si el color es propio.
        for(u32 w = 0; w < G->vertices[z].grado; w++) {
            //Si el color del vertice es igual al color de algunos de sus vecinos no es dos color.
            if(G->vertices[z].color == G->vertices[G->vertices[z].vecinos[w].posicionVertices].color) {
                return -1;
            }
        }
    }
    return 1;
}


/*-------------IMPLEMENTACIÓN DE LAS FUNCIONES AUXILIARES PARA CARGAR EL GRAFO--------------*/


//Crea un nuevo vertice, a partir de la standard input, para agregarlo al grafo.
VerticeSt crearVertice(u32 verticeNombre, u32 posicion, u32 grado) {

    VerticeSt vertice;
    vertice.nombre = verticeNombre;
    vertice.color = 0;
    vertice.grado = grado;
    vertice.posicion = posicion;
    vertice.vecinos = calloc(grado, sizeof(struct Vecino));

    return vertice;
}


// Obtiene un vertice de un grafo a partir de una posicion dada.
VerticeSt obtenerVertice(NimheP G, u32 pos) {

    return G->vertices[pos];
}

// Sobreescribe en un grafo un vertice dada su posicion.
void guardarVertice(NimheP G, VerticeSt vertice, u32 pos) {

    G->vertices[pos] = vertice;
    G->longitudActual++;
}

void DestruirLados(LadosP l) {
    free(l->arrayLados);
    l->arrayLados = NULL;
    free(l);
    l = NULL;
}

int cmpIzquierdos(const void *a, const void *b) {
    const struct Lado *x = a;
    const struct Lado *y = b;

    int comparar = x->izquierda - y->izquierda;

    return comparar;
}

int cmpDerechos(const void *a, const void *b) {
    const struct Lado *x = a;
    const struct Lado *y = b;

    int comparar = x->derecha - y->derecha;

    return comparar;
}

//guarda un elemento dado en el array de orden interno dentro de la estructura nimhep.
void GuardarOrden (NimheP grafo, OrdenSt orden, u32 pos) {

    grafo->arrayOrden[pos] = orden;
}

//Llena completamente el array orden a partir del array vertices dentro de la estructura nimhe.
void llenarArrayOrden(NimheP grafo) {

    OrdenSt orden;
    VerticeSt vertice;
    for(u32 i = 0; i < grafo->numeroVertices; i++) {
        vertice = obtenerVertice(grafo, i);
        orden.nombre = vertice.nombre;
        orden.color = vertice.color;
        orden.grado = vertice.grado;
        orden.posicion = vertice.posicion;
        orden.vmismocolor = 0;
        GuardarOrden (grafo, orden, i);
    }
}

//compara dos elementos del array orden por su nombre y retorna 1 si estan en orden creciente y -1 si no.
int cmpNombre(const void *a, const void *b) {
    const struct Orden *x = a;
    const struct Orden *y = b;

    int comparar = x->nombre - y->nombre;

    return comparar;
}

int cmpGradoDecreciente(const void *a, const void *b) {
    const struct Orden *x = a;
    const struct Orden *y = b;

    int comparar = y->grado - x->grado;

    return comparar;
}


int cmpColorDecreciente(const void *a, const void *b) {
    const struct Orden *x = a;
    const struct Orden *y = b;

    int comparar = y->color - x->color;

    return comparar;
}

int cmpCantColorCreciente(const void *a, const void *b) {
    const struct Orden *x = a;
    const struct Orden *y = b;

    int comparar = x->vmismocolor - y->vmismocolor;

    return comparar;
}

int cmpCantColorDecreciente(const void *a, const void *b) {
    const struct Orden *x = a;
    const struct Orden *y = b;

    int comparar = y->vmismocolor - x->vmismocolor;

    return comparar;
}


int cmpColor(const void *a, const void *b)
{
   return ( *(u32*)a - *(u32*)b );
}

int cmpColorOrden(const void *a, const void *b) {
    const struct Orden *x = a;
    const struct Orden *y = b;

    int comparar = x->color - y->color;

    return comparar;
}

//Imprime el orden del arrayOrden segun el nombre de los vertices.
void ImprimirOrdenArray(NimheP G) {
    printf("Color \n");
    for (u32 i = 0; i < G->numeroVertices; i++) {
        printf("%u, ", G->arrayOrden[i].color);
    }
    printf("\n");
    printf("Nombre \n");
    for (u32 i = 0; i < G->numeroVertices; i++) {
        printf("%u, ", G->arrayOrden[i].nombre);
    }
    printf("\n");
    printf("Posicion \n");
    for (u32 i = 0; i < G->numeroVertices; i++) {
        printf("%u, ", G->arrayOrden[i].posicion);
    }
    printf("\n");
    printf("Cantidad de vertices con el mismo color \n");
    for (u32 i = 0; i < G->numeroVertices; i++) {
        printf("%u, ", G->arrayOrden[i].vmismocolor);
    }
    printf("\n");
}

/*
    Funcion auxiliar usada para descolorear vértices. Es empleada en la funcion Chidos.
*/
void DescolorearVertices(NimheP G) {
    for(u32 i = 0; i < G->numeroVertices; i++) {
        G->vertices[i].color = 0;
    }
}

/*
    Funcion auxiliar que se usará en OrdenAleatorio. Se trata de un simple swap entre posiciones del array.
    Esta servira para intercambiar posiciones aleatorias que seran obtenidas mediante la funcion rand();
*/
void intercambioPosiciones(NimheP G, u32 i, u32 j) {
    OrdenSt tmp = G->arrayOrden[i];
    G->arrayOrden[i] = G->arrayOrden[j];
    G->arrayOrden[j] = tmp;
}
