#include <stdio.h>

void printArray(int * array, int size){

  int i;
  printf("[ ");
  for (i = 0; i < size; i++)
    printf("%d ", array[i]);
  printf("]\n");
}

int findLargestNum(int * array, int size){

  int i;
  int largestNum = -1;

  for(i = 0; i < size; i++){
    if(array[i] > largestNum)
      largestNum = array[i];
  }

  return largestNum;
}

// Radix Sort
void radixSort(int * array, int size){

  printf("\n\nRunning Radix Sort on Unsorted List!\n\n");

  int i;
  int semiSorted[size];
  int significantDigit = 1;
  int largestNum = findLargestNum(array, size);

  while (largestNum / significantDigit > 0){

    printf("\tSorting: %d's place ", significantDigit);

    int bucket[10] = { 0 };

    for (i = 0; i < size; i++)
      bucket[(array[i] / significantDigit) % 10]++;

    for (i = 1; i < 10; i++)
      bucket[i] += bucket[i - 1];

    for (i = size - 1; i >= 0; i--)
      semiSorted[--bucket[(array[i] / significantDigit) % 10]] = array[i];


    for (i = 0; i < size; i++)
      array[i] = semiSorted[i];

    significantDigit *= 10;


  }
}

int main(){

  printf("\n\nRunning Radix Sort Example in C!\n");
  printf("----------------------------------\n");

  int size = 12;
  int list[] = {2, 4, 4};   // aca va el array


  radixSort(&list[0], size);

  printf("\nSorted List:");
  printArray(&list[0], size);
  printf("\n");

  return 0;
}
